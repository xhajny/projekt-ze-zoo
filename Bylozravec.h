//
// Created by petrk on 6/1/2020.
//

#ifndef PROJEKT_ZE_ZOO_BYLOZRAVEC_H
#define PROJEKT_ZE_ZOO_BYLOZRAVEC_H

#include <iostream>
#include "Zvire.h"
#include "Savana.h"


class Bylozravec : public Zvire {

public:
    Bylozravec(std::string jmeno, int hmotnost);

    bool utec(Zvire *zvire);

    bool jeMasozravec();

    void nakrm(int jidlo, int voda);

    void printInfo();

    bool sezer(Zvire *korist);

};

#endif //PROJEKT_BYLOZRAVEC_H
