//
// Created by petrk on 6/1/2020.
//

#include "Masozravec.h"

Masozravec::Masozravec(std::string jmeno, int hmotnost) : Zvire(jmeno, hmotnost) {

}



void Masozravec::printInfo() {
    if (!jeMrtve()) {
        std::cout << "Druh: " << m_jmeno << std::endl;
        std::cout << "Jidlo: " << m_jidlo << std::endl;
        std::cout << "Voda: " << m_voda << std::endl;
        std::cout << "Hmotnost: " << m_hmotnost << std::endl;
        std::cout << "---------------------" << std::endl;
    } else {
        std::cout << m_jmeno << " je mrtve" << std::endl;
        std::cout << "---------------------" << std::endl;
    }
}

bool Masozravec::sezer(Zvire *korist) {
    std::cout << m_jmeno << " sezral " << korist->getJmeno() << std::endl;
    std::cout << "---------------------" << std::endl;
    if (korist->utec(this)) {
        m_jidlo-= 25;
        return false;
    } else {
        if (m_jidlo + korist->getJidlo() <= 100) m_jidlo += korist->getJidlo();
        if (m_jidlo + korist->getVoda() <= 100) m_voda += korist->getVoda();
        delete korist;
        return true;
    }
}

bool Masozravec::jeMasozravec() {
    return true;
}

void Masozravec::nakrm(int jidlo, int voda) {
    if (m_jidlo + jidlo <= 100) m_jidlo += jidlo;
    if (m_voda + voda <= 100) m_voda += voda;
}

bool Masozravec::utec(Zvire *zvire) {
    return m_hmotnost > zvire->getHmotnost();
}
