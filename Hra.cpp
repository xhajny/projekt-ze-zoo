//
// Created by Mate on 04-Jun-20.
//

#include "Hra.h"

void Hra::spustiHru() {
    Mapa *mapa = Mapa::getMapa();
    mapa->vypisMapu();
    std::string druh;
    int pocetZvirat = 0;
    std::cout << "Zvol si pocet zvirat: ";
    std::cin >> pocetZvirat;

    for (int i = 0; i < pocetZvirat; i++) {
        std::cout << "Zvol si druh M/B: ";
        std::cin >> druh;

        Zvire *zvire = nullptr;

        zvire = Zvire::getZvire(druh);
        mapa->ulozNaPozici((rand() % 5), (rand() % 5), zvire); //vložení zvířete na nahodou pozici na mapě při startu.
    };
    int kol = 0;
    std::cout << "Zvol si pocet kol: ";
    std::cin >> kol;

    for (int i = 0; i < kol; i++) {
        mapa->vypisMapu();
        mapa->pohybZvirat();
        mapa->akceZvirat();
    };
    mapa->vypisMapu(); //poslední vypsání mapy po ukončení všech kol


}