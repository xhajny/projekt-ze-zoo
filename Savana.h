//
// Created by Mate on 04-Jun-20.
//

#ifndef PROJEKT_ZE_ZOO_SAVANA_H
#define PROJEKT_ZE_ZOO_SAVANA_H
#include "Pole.h"

class Savana : public Pole {

    int m_zmenJidlo;
    int m_zmenVodu;

public:

    Savana();


    int zmenJidlo();

    int zmenVodu();


    std::string getZnacka();

};







#endif //PROJEKT_ZE_ZOO_SAVANA_H
