//
// Created by Mate on 04-Jun-20.
//

#ifndef PROJEKT_ZE_ZOO_POLE_H
#define PROJEKT_ZE_ZOO_POLE_H

#include <iostream>
#include <vector>
#include <algorithm>
#include "Zvire.h"

class Pole {
    std::vector<Zvire *> m_zvirata;
    std::vector<Pole *> m_poleVOkoli;

    Pole *getNahodnePoleZOkoli();

public:
    void setPoleVOkoli(std::vector<Pole *> poleVOkoli);

    bool obsahujeZvire();

    void pridejZvire(Zvire *zvirata);

    virtual std::string getZnacka() = 0;

    void interakceZvirete();

    void posunZvirete(std::vector<Zvire *> *jizPosunutaZvirata);

    std::vector<Zvire *> &getPoleZvirat();


    virtual int zmenVodu() = 0;

    virtual int zmenJidlo() = 0;

    void nakrmZvire();
};

#endif //PROJEKT_ZE_ZOO_POLE_H
