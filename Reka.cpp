//
// Created by Mate on 04-Jun-20.
//

#include "Reka.h"

Reka::Reka() {
    m_zmenVodu = +40;

    m_zmenJidlo = -30;
};

int Reka::zmenVodu() {
    return m_zmenVodu;

}

int Reka::zmenJidlo() {
    return m_zmenJidlo;
}



std::string Reka::getZnacka() {
    std::string znacka = "";
    if (obsahujeZvire()) {
        for (Zvire *zvire:Pole::getPoleZvirat()) {
            znacka += zvire->getZnacka();
        }
    } else {
        znacka += "*";
    }
    return znacka;
}