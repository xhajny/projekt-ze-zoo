//
// Created by petrk on 6/1/2020.
//

#include "Bylozravec.h"


Bylozravec::Bylozravec(std::string jmeno, int hmotnost) : Zvire(jmeno, hmotnost) {

}

bool Bylozravec::utec(Zvire *zvire) { // Šance  50% na to že uteče
    if ((rand() % 100) > 50 ) {
        return true;
    } else {
        return false;
    }
}

void Bylozravec::printInfo() { // vypis informaci o vlastnostech zvirete
    if (!jeMrtve()) {
        std::cout << "Druh: " << m_jmeno << std::endl;
        std::cout << "Jidlo: " << m_jidlo << std::endl;
        std::cout << "Voda: " << m_voda << std::endl;
        std::cout << "Hmotnost: " << m_hmotnost << std::endl;
        std::cout << "----------------------------" << std::endl;
    } else {

        std::cout << m_jmeno << " umrelo" << std::endl;
        std::cout << "----------------------------" << std::endl;
    }
}

bool Bylozravec::jeMasozravec() {
    return false;
}

void Bylozravec::nakrm(int jidlo, int voda) { // přidavani jidla a vody a ověření, jestli jsou hodnoty kladné
    if (m_jidlo + jidlo <= 100) m_jidlo += jidlo;
    if (m_voda + voda <= 100) m_voda += voda;
    if (m_jidlo <= 0)m_jidlo = 0;
    if (m_voda <= 0)m_voda = 0;
}



bool Bylozravec::sezer(Zvire *korist) {
    return false;
}