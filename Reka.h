//
// Created by Mate on 04-Jun-20.
//

#ifndef PROJEKT_ZE_ZOO_REKA_H
#define PROJEKT_ZE_ZOO_REKA_H


#include <iostream>
#include "Pole.h"


class Reka : public Pole {
    int m_zmenEnergii;
    int m_zmenJidlo;
    int m_zmenVodu;
public:
    Reka();

    int zmenJidlo();

    int zmenVodu();

    int zmenEnergii();

    std::string getZnacka();

};


#endif //PROJEKT_ZE_ZOO_REKA_H
