//
// Created by petrk on 6/1/2020.
//


#include "Zvire.h"
#include "Masozravec.h"
#include "Bylozravec.h"

Zvire::Zvire(std::string jmeno, int hmotnost) {
    m_jmeno = jmeno;
    m_hmotnost = hmotnost;
    m_jidlo = 100;
    m_voda = 100;

}


std::string Zvire::getZnacka() {
    // M oznacime mrtva zvirata
    if (jeMrtve()) {
        return "M";
    }
        // Z oznacime ziva zvirata
    else {
        return "Z";
    }
}

Zvire::~Zvire(){

}

Zvire *Zvire::getZvire(std::string druh) {
    if (druh == "B") {
        std::string druhBylozravce;
        std::cout << "Vyber si bylozravce (L)uskoun/(Z)ebra: " << std::endl;
        std::cin >> druhBylozravce;
        if (druhBylozravce == "Z") {
            return new Bylozravec("Zebra", 100);
        } else if (druhBylozravce == "L") {
            return new Bylozravec("Luskoun", 50);
        } else {
            std::cerr << "Chybne vlozeni, vytvorim Luskouna." << std::endl;
            return new Bylozravec("Lusknou", 50);
        }
    } else if (druh == "M") {
        std::string druhMasozravce;
        std::cout << "Vyber si masozravce (J)aguar/(V)aran: " << std::endl;
        std::cin >> druhMasozravce;
        if (druhMasozravce == "V") {
            return new Masozravec("Varan", 200);
        } else if (druhMasozravce == "J") {
            return new Masozravec("Jaguar", 180);
        } else {
            std::cerr << "Tento druh neexistuje, vytvarim Jaguara." << std::endl;
            return new Masozravec("Jaguar", 180);
        }
    } else {
        std::cerr << "Chybne vlozeni, vytvorim Varana." << std::endl;
        return new Masozravec("Varan", 200);
    }
}

bool Zvire::jeMrtve() {
    if (m_jidlo <= 0 || m_voda <= 0 ) {
        return true;
    } else {
        return false;
    }
}

int Zvire::getJidlo() {
    return m_jidlo;
}

int Zvire::getVoda() {
    return m_voda;
}

std::string Zvire::getJmeno() {
    return m_jmeno;
}

int Zvire::getHmotnost() {
    return m_hmotnost;
}

void Zvire::umri() {
    delete (this);
}
