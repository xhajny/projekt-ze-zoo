//
// Created by petrk on 6/1/2020.
//

#ifndef PROJEKT_ZE_ZOO_MASOZRAVEC_H
#define PROJEKT_ZE_ZOO_MASOZRAVEC_H

#include "Zvire.h"
#include "Pole.h"
#include <iostream>

class Masozravec : public Zvire {

public:
    Masozravec(std::string jmeno, int hmotnost);

    bool sezer(Zvire *korist);



    bool jeMasozravec();

    void printInfo();

    void nakrm(int jidlo, int voda);

    bool utec(Zvire *zvire);
};


#endif //PROJEKT_ZE_ZOO_MASOZRAVEC_H
