//
// Created by Mate on 04-Jun-20.
//

#include "Savana.h"

Savana::Savana() {
    m_zmenVodu = -30;

    m_zmenJidlo = +20;
}

int Savana::zmenVodu() {
    return m_zmenVodu;
}

int Savana::zmenJidlo() {
    return m_zmenJidlo;
}

std::string Savana::getZnacka() {
    std::string znacka = "";
    if (obsahujeZvire()) {
        for (Zvire *zvire:Pole::getPoleZvirat()) {
            znacka += zvire->getZnacka();
        }
    } else {
        znacka += "X";
    }
    return znacka;
}